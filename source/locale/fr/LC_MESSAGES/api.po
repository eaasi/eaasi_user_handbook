#
msgid ""
msgstr ""
"Project-Id-Version: 1.0\n"
"Report-Msgid-Bugs-To: EaaSI\n"
"POT-Creation-Date: 2024-01-31 11:51-0500\n"
"PO-Revision-Date: 2024-01-31 11:51-0500\n"
"Last-Translator: Argos-Translate\n"
"Language-Team: EaaSI\n"
"Language: fr\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=utf-8\n"
"Content-Transfer-Encoding: 8bit\n"

msgid "EaaSI API"
msgstr "API EaSI"

msgid ""
"Emulation-as-a-Service exposes a number of RESTful API endpoints users can "
"employ with an API client (including ``curl`` or any other HTTP request "
"tooling) to get information and interact with the resources present in an "
"EaaSI node."
msgstr ""
"Emulation-as-a-Service expose un certain nombre d'utilisateurs RESTful API "
"endpoints peuvent utiliser avec un client API (y compris ``curl```` ou tout "
"autre outil de requête HTTP) pour obtenir des informations et interagir avec"
" les ressources présentes dans un noeud EaSI."

msgid "The EaaS/EaaSI API can be useful to:"
msgstr "L'API EaS/EaSI peut être utile pour:"

msgid "query and gather metadata"
msgstr "recherche et collecte de métadonnées"

msgid "interact with the experimental \"UVI\"; see :ref:`uvi`"
msgstr "interagir avec le \"UVI\" expérimental; voir :ref:`uvi`"

msgid ""
"control and perform resource exchange between EaaSI nodes via OAI-PMH (it is"
" recommended to interact with OAI-PMH endpoints via the EaaSI UI's built-in "
"functionality, see :ref:`oaipmh` and :ref:`oai-pmh_management`)"
msgstr ""
"contrôler et effectuer l'échange de ressources entre les nœuds EaSI via OAI-"
"PMH (il est recommandé d'interagir avec les paramètres OAI-PMH via la "
"fonctionnalité intégrée de l'UI EaSI, voir :ref:`oaipmh` et :ref:`oai-"
"pmh_management`)"

msgid "Authentication and Access Tokens"
msgstr "Jetons d'authentification et d'accès"

msgid ""
"EaaS's public API can return information about Environments and Software "
"present in a specified EaaSI node. Users with an account in that node can "
"query the API based on their unique user context (for example, to receive "
"metadata about Private Environments or imported Software resources) by "
"obtaining an access token from EaaSI's `Keycloak "
"<https://www.keycloak.org/>`_ authentication module."
msgstr ""
"L'API publique d'EaaS peut renvoyer des informations sur Environnements et "
"Logiciels présents dans un noeud EaSI spécifié. Les utilisateurs ayant un "
"compte dans ce noeud peuvent demander l'API en fonction de leur contexte "
"d'utilisateur unique (par exemple, pour recevoir des métadonnées sur les "
"environnements privés ou les ressources logicielles importées) en obtenant "
"un jeton d'accès du module d'authentification d'EaaSI `Keycloak ^."

msgid ""
"If no access token/user context is provided, GET requests to the public API "
"may still return metadata on a node's published/public resources (Public "
"Environments)"
msgstr ""
"Si aucun contexte de jeton d'accès ou d'utilisateur n'est fourni, les "
"demandes de GET à l'API publique peuvent toujours renvoyer des métadonnées "
"sur les ressources publiées ou publiques d'un noeud (Environnements publics)"

msgid ""
"Users with an EaaSI account can acquire an access token from their node by "
"sending a POST request to "
"``https://[eaasi.domain]/auth/realms/master/protocol/openid-connect/token``,"
" for example:"
msgstr ""
"Les utilisateurs d &apos; un compte EaSI peuvent acquérir un jeton d &apos; "
"accès à partir de leur nœud en envoyant une requête POST à "
"``https://[eaasi.domain]/auth/realms/master/protocol/openid-connect/token`, "
"par exemple:"

msgid ""
"Being sure to replace all information in ``[]`` with the appropriate "
"values/credentials. Tokens are valid for an hour by default, after which "
"they must be regenerated or refreshed."
msgstr ""
"Être sûr de remplacer toutes les informations dans ``[]` avec les "
"valeurs/credentials appropriés. Les jetons sont valables pour une heure par "
"défaut, après quoi ils doivent être régénérés ou rafraîchis."

msgid ""
"A valid request should receive an `OAuth 2.0 access token response "
"<https://www.oauth.com/oauth2-servers/access-tokens/access-token-"
"response/>`_. The received ``access_token`` property value can then be "
"provided in the header of subsequent requests to the EaaS API, for example:"
msgstr ""
"Une demande valide devrait recevoir une réponse de jeton d &apos; accès &lt;"
" &lt; OAuth 2.0 &gt; &gt; . https://www.oauth.com/oauth2-servers/access-"
"tokens/access-token-response/ю_. La valeur de propriété reçue "
"``access_token` peut ensuite être fournie dans l`en-tête des requêtes "
"ultérieures à l`API EaS, par exemple:"

msgid "Public API"
msgstr "API publique"

msgid ""
"**All public EaaS API endpoints are located under the base URL:** "
"``https://[eaasi.domain]/emil``"
msgstr ""
"**Tous les paramètres de l'API public EaS sont situés sous l'URL de base:** "
"``https://[eaasi.domain]/emil```"

msgid "Environments"
msgstr "Environnements"

msgid "Retrieves a list of all available Environments in the node"
msgstr ""
"Récupère une liste de tous les environnements disponibles dans le noeud"

msgid "Request Headers"
msgstr "Demande Headers"

msgid "Should define response content type as `application/json`"
msgstr ""
"Devrait définir le type de contenu de réponse comme `application/json`"

msgid ""
"optional OAuth token to authenticate (necessary to retrieve Private "
"Environments)"
msgstr ""
"jeton OAuth optionnel pour authentifier (nécessaire pour récupérer les "
"environnements privés)"

msgid "Query Parameters"
msgstr "Paramètres de requête"

msgid ""
"If set to \"true\", can be used to display *all* metadata associated with "
"each Environment rather than an overview (see ``environment-"
"repository/environments/[envId]`` endpoint documentation below for full "
"details included; \"false\" by default)"
msgstr ""
"Si l &apos; on s &apos; applique à la &quot; vérité &quot; , il est possible"
" d &apos; afficher des métadonnées *toutes* associées à chaque environnement"
" plutôt qu &apos; une vue d &apos; ensemble (voir ``environnement-"
"repository/environments/[envId] &apos; &apos; &apos; extrémité documentation"
" ci-dessous pour tous les détails inclus; &quot; false &quot; par défaut)"

msgid ""
"If set to \"false\", returns metadata associated with Public Environments "
"visible but **not** Saved Locally to the node (\"true\" by default)"
msgstr ""
"S'il s'agit de \"false\", renvoie les métadonnées associées aux "
"Environnements Publics visibles mais **non** sauvegardées Localement au "
"noeud (\"true\" par défaut)"

msgid "Status Codes"
msgstr "Codes de statut"

msgid "An array of Environments"
msgstr "Un éventail d'environnements"

msgid "Response JSON Object"
msgstr "Réponse JSON Objet"

msgid "Unique ID (UUID) for an Environment resource"
msgstr "ID unique (UUID) pour une ressource environnementale"

msgid "Human-readable name for an Environment resource"
msgstr "Nom humain lisible pour une ressource environnementale"

msgid ""
"Indicates the Environment's storage archive, one of three options depending "
"on its Network Status (returns \"remote\" for Public, \"public\" for Saved "
"Locally, \"default\" for Private)"
msgstr ""
"Indique l'archive de stockage de l'environnement, l'une des trois options en"
" fonction de son statut réseau (retourne \"remote\" pour Public, \"public\" "
"pour Saved Locally, \"default\" pour Private)"

msgid ""
"Indicates user account that owns the Environment resource (either returns "
"\"shared\" for Public or Saved Locally, or a UUID for provided user "
"account's Private Environments)"
msgstr ""
"Indique le compte utilisateur qui possède la ressource Environnement (qu'il "
"retourne « partagé » pour Public ou Saved Locally, ou un UUID pour les "
"environnements privés du compte utilisateur fourni)"

msgid ""
"If Environment is a :term:`Content Environment`, returns the UUID of the "
"associated :term:`Content` object"
msgstr ""
"Si Environnement est un :term:`Content Environment`, retourne l`UUUUID de "
"l`objet associé:term:`Content`"

msgid ""
"If Environment is a :term:`Content Environment`, returns the UUID of the "
"user archive associated with that :term:`Content` (should be the same as "
"``owner`` UUID)"
msgstr ""
"Si Environnement est un :term:`Content Environment`, retourne l`UUUUID de "
"l`archive utilisateur associée à ce:term:`Content` (devrait être le même que"
" ``propriété`` UUID)"

msgid ""
"Returns \"object\" if Environment is a :term:`Content Environment`, returns "
"\"base\" for all others"
msgstr ""
"Retourne \"objet\" si Environnement est un :term:`Content Environment`, "
"retourne \"base\" pour tous les autres"

msgid "ISO 8601 full-time timestamp for when the Environment was created"
msgstr "ISO 8601 Temps plein temps pour la création de l'environnement"

msgid ""
"The most recent description of the Environment from its History (displayed "
"during running Emulation Access sessions)"
msgstr ""
"La description la plus récente de l'environnement de son histoire "
"(découverte lors de sessions d'Emulation Access)"

msgid ""
"Indicates if the Environment is a Linux runtime appropriate for importing "
"and running containers (an EaaS feature not yet implemented in EaaSI nodes)"
msgstr ""
"Indique si l'environnement est un temps d'exécution Linux approprié pour "
"l'importation et l'exécution de conteneurs (une fonction EaS n'est pas "
"encore mise en œuvre dans les noeuds EaSI)"

msgid ""
"Indicates if the Environment is capable of networking with the live internet"
" or other Environments (the latter is an EaaS feature not yet implemented in"
" EaaSI nodes)"
msgstr ""
"Indique si l'environnement est capable de réseautage avec l'Internet en "
"direct ou d'autres environnements (ce dernier est une fonctionnalité EaS pas"
" encore mise en œuvre dans les noeuds EaSI)"

msgid ""
"Indicates if the Environment is allowed to connect to the live internet (can"
" only be \"true\" if ``networkEnabled`` is also \"true\")"
msgstr ""
"Indique si l'environnement est autorisé à se connecter à l'internet en "
"direct (ne peut être \"true\" que si \"networkEnabled\" est également "
"\"true\")"

msgid ""
"Indicates if the Environment represents a container service (an EaaS feature"
" not yet implemented in EaaSI nodes)"
msgstr ""
"Indique si l'environnement représente un service de conteneurs (une "
"fonctionnalité EaS qui n'est pas encore mise en œuvre dans les nœuds EaSI)"

msgid "**Example response**:"
msgstr "**Exemple response**:"

msgid ""
"Retrieves a detailed record of a single Environment, as defined by a "
"provided UUID"
msgstr ""
"Retrieves a detailed record of a single Environment, as defined by a "
"provided UUID"

msgid "A single Environment JSON object"
msgstr "Un seul objet Environnement JSON"

msgid "No Environment with the specified UUID found"
msgstr "Aucun environnement avec l'UUID spécifié trouvé"

msgid ""
"a nested JSON object literal describing the Environment's emulated "
"networking capabilities"
msgstr ""
"a nested JSON object literal décrivant les capacités de réseautage émulées "
"de l'environnement"

msgid "*EaaS feature not yet implemented in EaaSI nodes*"
msgstr ""
"*La fonctionnalité EaS n'est pas encore mise en œuvre dans les nœuds EaSI*"

msgid ""
"If the Environment is a :term:`derivative`, this references the unique ID "
"(UUID) of the base Environment this derivative was directly saved from"
msgstr ""
"Si l'environnement est un :term:`derivative`, cette référence l'identifiant "
"unique (UUUID) de la base Environnement ce dérivé a été directement sauvé de"

msgid "Unique ID (UUID) for the Environment resource"
msgstr "ID unique (UUID) pour les ressources environnementales"

msgid "Human-readable name for the Environment resource"
msgstr "Nom humain lisible de la ressource pour l &apos; environnement"

msgid ""
"`JavaBeans <https://en.wikipedia.org/wiki/JavaBeans>`_  bean name for the "
"emulator used by this Environment resource"
msgstr ""
"`JavaBeans Őhttps://en.wikipedia.org/wiki/JavaBeans epi`_ bean name for the "
"emulator used by this Environment resource"

msgid ""
"When \"true\", enables the \"Relative Mouse (Pointerlock)\" setting (see: "
":ref:`environment_details`)"
msgstr ""
"Quand \"true\", permet le réglage \"Relative Mouse (Pointerlock)\" "
"(voir:ref:`environment_details`)"

msgid ""
"When \"true\", enables the \"Environment Can Print\" setting (see: "
":ref:`environment_details`)"
msgstr ""
"Quand \"true\", permet le réglage \"Environment Can Print\" "
"(voir:ref:`environment_details`)"

msgid ""
"When \"true\", enables the \"Requires Clean Shutdown\" setting (see: "
":ref:`environment_details`)"
msgstr ""
"Quand \"true\", permet le réglage \"Requires Clean Shutdown\" "
"(voir:ref:`environment_details`)"

msgid ""
"When ``useXpra`` is set to \"true\", this string determines the type of "
"image encoding used by the Xpra display server and client (\"jpeg\" by "
"default)"
msgstr ""
"Lorsque ``useXpra` est défini à \"true\", cette chaîne détermine le type "
"d'encodage d'image utilisé par le serveur d'affichage Xpra et le client "
"(\"jpeg\" par défaut)"

msgid ""
"If the selected Environment is a :term:`derivative`, a sub-array of the "
"previous revisions in the Environment's History (listed from most recent to "
"oldest revisions)"
msgstr ""
"Si l'environnement sélectionné est un :term:`derivative`, un sub-array des "
"révisions précédentes dans l'histoire de l'environnement (listé des "
"révisions les plus récentes aux plus anciennes)"

msgid "``envId`` for the previous Environment in the derivative chain"
msgstr "``envId` pour l`environnement précédent dans la chaîne dérivée"

msgid ""
"The ``description`` for the previous Environment in the derivative chain"
msgstr ""
"&lt; &lt; escale &gt; &gt; pour l &apos; environnement précédent dans la "
"chaîne dérivée"

msgid "The ``archive`` for the previous Environment in the derivative chain"
msgstr "Le ``archive` pour l`environnement précédent dans la chaîne dérivée"

msgid ""
"A sub-array of any Software resources associated with this Environment. "
"Could include an operating system installer if the Environment is a "
":term:`base` or other associated Software resources if the Environment is a "
":term:`derivative`"
msgstr ""
"Un sous-réseau de toutes les ressources logicielles associées à cet "
"environnement. Peut inclure un installateur de système d'exploitation si "
"l'environnement est un :term:`base` ou d'autres ressources logicielles "
"associées si l'environnement est un:term:`derivative `"

msgid "The ``id`` of any associated Software resource"
msgstr "Le ``id``` de toute ressource logicielle associée"

msgid "The human-readable name of any associated Software resource"
msgstr "Le nom humain lisible de toute ressource logicielle associée"

msgid ""
"The ``archiveId`` for the storage archive where the associated Software "
"resource is kept"
msgstr ""
"Le ``archiveId`` pour les archives de stockage où la ressource logicielle "
"associée est conservée"

msgid ""
"Arbitrary string/flags added to configured emulator's default when "
"Environment is run (see: Emulator Configuration)"
msgstr ""
"Chaîne/flags Arbitraires ajoutés à la configuration par défaut de "
"l'émulateur lorsque Environnement est exécuté (voir : Configuration de "
"l'émulateur)"

msgid ""
"When \"true\", enables the \"XPRA Video\" setting (see: "
":ref:`environment_details`)"
msgstr ""
"Quand \"true\", permet le réglage \"XPRA Video\" "
"(voir:ref:`environment_details`)"

msgid ""
"When \"true\", enables the \"WebRTC Audio\" setting (see: "
":ref:`environment_details`)"
msgstr ""
"Quand \"true\", permet le réglage \"WebRTC Audio\" "
"(voir:ref:`environment_details`)"

msgid ""
"Name for the :ref:`Docker container <emulators>` matching the specified "
"emulator bean"
msgstr ""
"Nom pour le :ref:`Docker container observaulatorsю` matching the specified "
"emulator bean"

msgid ""
"A sub-array of JSON objects, each represeting the emulated drives available "
"for this Environment"
msgstr ""
"Un sub-array d'objets JSON, chaque réinitialisation des lecteurs émulés "
"disponibles pour cet environnement"

msgid ""
"If an :term:`object` or environment image is bound to this drive, its unique"
" ID (UUID) will be indicated in this field (i.e. system drives or a "
":term:`Content Environment`)"
msgstr ""
"Si une image de :term:`object` ou de l`environnement est liée à ce disque, "
"son ID unique (UUUID) sera indiqué dans ce champ (c.-à-d. entraînements "
"système ou un:term:`Content Environment`)"

msgid ""
"Computer storage standard/interface used by the emulated drive (options are "
"\"ide\" or \"floppy\")"
msgstr ""
"Norme de stockage d'ordinateur/interface utilisée par le lecteur émulé (les "
"options sont \"ide\" ou \"floppy\")"

msgid "Computer bus address number used by the emulated drive"
msgstr "Numéro d'adresse de bus informatique utilisé par le lecteur émulé"

msgid "Computer storage bus unit address number used by the emulated drive"
msgstr ""
"Numéro d'adresse de l'unité de stockage d'ordinateur utilisé par le lecteur "
"émulé"

msgid ""
"Corresponding Physical Media type for the emulated drive (allows which type "
"of resource objects can be loaded into this drive; accepted values are "
"\"floppy\", \"cdrom\", or \"disk\")"
msgstr ""
"Correspondant Type de médias physiques pour le lecteur émulé (selon quel "
"type d'objets de ressources peut être chargé dans ce lecteur; les valeurs "
"acceptées sont \"floppy\", \"cdrom\", ou \"disk\")"

msgid ""
"Indicates the filesystem types accepted by the emulated drive (e.g. \"ISO\" "
"for \"cdrom\" drives or \"fat12\" for \"floppy\")"
msgstr ""
"Indique les types de systèmes de fichiers acceptés par le lecteur émulé (par"
" exemple \"ISO\" pour les disques \"cdrom\" ou \"fat12\" pour \"floppy\")"

msgid ""
"Communicates to the emulator (if possible) that this drive should be "
"indicated as the system/boot drive (only one drive in the Environment "
"configuration should be marked \"true\", otherwise behavior will be "
"unpredictable)"
msgstr ""
"Communique à l'émulateur (si possible) que ce disque doit être indiqué comme"
" le système/boot drive (seulement un lecteur dans la configuration "
"Environnement devrait être marqué \"true\", sinon le comportement sera "
"imprévisible)"

msgid ""
"Indicates if the Environment is a Linux runtime appropriate for importing "
"and running containers (*EaaS feature not yet implemented in EaaSI nodes*)"
msgstr ""
"Indique si l'environnement est un temps d'exécution Linux approprié pour "
"l'importation et l'exploitation de conteneurs (*La fonctionnalité EaS n'est "
"pas encore implémentée dans les nœuds EaSI*)"

msgid ""
"Indicates if the Environment represents a container service (*EaaS feature "
"not yet implemented in EaaSI nodes*)"
msgstr ""
"Indique si l'environnement représente un service de conteneurs (*La "
"fonctionnalité EaS n'est pas encore mise en œuvre dans les nœuds EaSI*)"

msgid "**Example response:**"
msgstr "**Réponse exemplaire :**"

msgid "Software"
msgstr "Logiciels"

msgid "Retrieves a lightweight list of Software resources"
msgstr "Récupère une liste légère des ressources logicielles"

msgid ""
"OAuth token to authenticate (necessary to retrieve Private Software "
"resources)"
msgstr ""
"OAuth token to authenticate (necessaire de récupérer les ressources du "
"logiciel privé)"

msgid "An array of Software"
msgstr "Un éventail de logiciels"

msgid "Unique ID (UUID) for this Software resource"
msgstr "ID unique (UUID) pour cette ressource logicielle"

msgid "Human-readable name for a Software resource"
msgstr "Nom humain lisible pour une ressource logicielle"

msgid ""
"Indicates if the Software resource has been published (publishing Software "
"resources has not yet been properly implemented in EaaSI; this value should "
"be \"false\")"
msgstr ""
"Indique si la ressource logicielle a été publiée (la publication des "
"ressources logicielles n'a pas encore été correctement mise en œuvre dans "
"EaSI; cette valeur devrait être \"false\")"

msgid ""
"Name for the storage archive where the Software object is kept; should be "
"\"zero conf\" for all private Software resources"
msgstr ""
"Nom pour l'archive de stockage où l'objet Logiciel est conservé; doit être "
"\"zero conf\" pour toutes les ressources du Logiciel privé"

msgid ""
"Indicates if the Software resource has been identified as an operating "
"system installer during resource import (available via Demo UI but not yet "
"re-incorporated into EaaSI UI import process; should be \"false\" but "
"certain legacy objects may display \"true\")"
msgstr ""
"Indique si la ressource logicielle a été identifiée comme un installateur de"
" système d'exploitation lors de l'importation de ressources (disponible via "
"Demo UI mais pas encore réincorporée dans le processus d'importation EaSI "
"UI; devrait être \"false\" mais certains objets anciens peuvent afficher "
"\"true\")"

msgid ""
"Retrieves description for a single Software resource, as defined by provided"
" UUID"
msgstr ""
"Retrieves description for a single Software resource, as defined by provided"
" UUID"

msgid "A single Software JSON object"
msgstr "Un seul objet JSON logiciel"

msgid "No Software object with the specified UUID found"
msgstr "Aucun objet logiciel avec le UUID spécifié trouvé"

msgid "Human-readable name for this Software resource"
msgstr "Nom humain pour cette ressource logicielle"

msgid ""
"Indicates if this Software resource has been published (publishing Software "
"resources has not yet been properly implemented in EaaSI; this value should "
"be \"false\")"
msgstr ""
"Indique si cette ressource logicielle a été publiée (la publication des "
"ressources logicielles n'a pas encore été correctement mise en œuvre dans "
"EaSI; cette valeur devrait être \"false\")"

msgid ""
"Retrieves a more detailed array of Software objects, including associated "
"file formats"
msgstr ""
"Récupère une gamme plus détaillée d'objets logiciels, y compris les formats "
"de fichiers associés"

msgid "Unique ID (UUID) for this Software object (should be same as ``id``)"
msgstr ""
"ID unique (UUUID) pour cet objet logiciel (devrait être identique à ``id``)"

msgid "Open field for defining any relevant software license information"
msgstr "Open field for defining any relevant software license information"

msgid ""
"Defines how many *concurrent* emulation sessions may be run with this "
"Software object mounted (default is \"-1\", indicating *unlimited* sessions)"
msgstr ""
"Définit combien de sessions d'émulation *concurrent* peuvent être exécutées "
"avec cet objet logiciel monté (par défaut est \"-1\", indiquant *unlimited* "
"sessions)"

msgid ""
"An array of file format PUIDs that have been manually assigned with this "
"Software object as formats that can be natively rendered in Environments "
"associated with this Software resource (see :ref:`uvi`)"
msgstr ""
"Un tableau de format de fichier PUIDs qui ont été assignés manuellement avec"
" cet objet logiciel comme formats qui peuvent être rendus nativement dans "
"les environnements associés à cette ressource logicielle (voir :ref:`uvi`)"

msgid ""
"An array of file format PUIDs that have been manually assigned with this "
"Software object as formats that can specifically be *imported* in "
"Environments associated with this Software resource (primarily experimental,"
" for investigating automated migration)"
msgstr ""
"Un tableau de format de fichier PUIDs qui ont été assignés manuellement avec"
" cet objet logiciel comme formats qui peuvent être spécifiquement importés* "
"dans les environnements associés à cette ressource logicielle "
"(principalement expérimental, pour l'enquête sur la migration automatisée)"

msgid ""
"An array of file format PUIDs that have been manually assigned with this "
"Software object as formats that can specifically be *exported* from "
"Environments associated with this Software resource (primarily experimental,"
" for investigating automated migration)"
msgstr ""
"Un tableau de format de fichier PUIDs qui ont été assignés manuellement avec"
" cet objet logiciel comme formats qui peuvent être spécifiquement *exportés*"
" à partir d'Environnements associés à cette ressource logicielle "
"(principalement expérimental, pour l'enquête sur la migration automatisée)"

msgid ""
"Retrieves a more detailed view of a single Software resource, as defined by "
"provided UUID"
msgstr ""
"Retrieves a more detailed view of a single Software resource, as defined by "
"provided UUID"

msgid "Universal Virtual Interactor (Experimental)"
msgstr "Interacteur virtuel universel (Experimental)"

msgid "lorem ipsum"
msgstr "lorem ipsum"
