.. Dashboard

.. include:: ../sunset.rst

.. _dashboard:

Dashboard
==========

The EaaSI Dashboard provides a central hub for viewing :term:`resources <resource>` and activity within
your node, as well as links for additional support and materials from the EaaSI project.

.. image:: ../images/dashboard.png
    
**Sandbox Demo**: Will take the user to the `EaaSI Open Source Sandbox <https://sandbox.eaasi.cloud>`_, a demonstration service for 
showing the in-browser emulation capabilities of EaaS to those outside the EaaSI Network.
  
**Learn and Support**: Links to the EaaSI User Handbook that you are reading right now!

**User Forum**: Links to the `EaaSI Community Forum <https://forum.eaasi.cloud>`_, a public Discourse for reporting bugs to the development team, as well as connecting with other
EaaSI users for support and collaboration. (See :ref:`community`)
  
Activity
---------

The EaaSI Dashboard displays three activity feeds to conveniently access the latest environments and other
resources available in your EaaSI :term:`node`.

**My Node Activity** displays the latest Environments created or uploaded in your node, regardless of the
user who created them. (You can click on "See ALL Node Resources" to navigate to the :ref:`explore` page)

**My Resources** displays the latest Environments created, uploaded, or bookmarked by the currently logged-in
user. (You can click on "See ALL My Resources" to navigate to the :ref:`my_resources` page)
