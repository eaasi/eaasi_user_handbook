.. My Resources

.. include:: ../sunset.rst

.. _my_resources:

My Resources
=============

The My Resources page is unique for every EaaSI user account. It tracks and displays bookmarked and imported resources by the logged-in user.

As on the :ref:`explore` page, you can use the Refine Your Results page to sort quickly through either My Bookmarks or Imported Resources based on Resource Type and Network Status.

.. image:: ../images/my_resources.png

By default, the My Resources page will open on the "My Bookmarks" tab, but users can also navigate to the "Imported Resources" tab to view and track Software and Content resources that they have individually imported to the node:

.. image:: ../images/imported_resources.png
