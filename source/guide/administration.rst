.. Administrative settings

.. include:: ../sunset.rst

.. _administration:

Manage Node
**************

.. note::
  Though the Manage Node interface should be visible to both :term:`Admin` and :term:`Configuration Users <configuration user>`, Configuration Users will not see all the management functionality described on this page. This is intentional. Configuration Users can see Running Process and download logs for troubleshooting and support purposes, but will not be able to control node-level settings like user permissions, emulator management, or OAI-PMH endpoint synchronization.

The Manage Node page has two primary sections: :ref:`node_management` for controlling the :term:`node` itself (emulators, OAI-PMH, computing
resources) and :ref:`user_admin` for managing the users who can access the node. See each section below for more details.

This page also displays the node's EaaSI Application Version number, which is important information to include
when :ref:`bugs`.


.. _node_management:

Node Management
==================

.. _managing_emulators:

Emulators
-----------

The EaaSI platform relies on a number of open-source emulators. Replicating environments, troubleshooting,
and expanding a node's functionality all may involve some management of the emulators themselves.

:term:`Admin` users can review and manage emulators via the "Emulators" section of the Manage Node page.

.. image:: ../images/emulators_menu.png

The list on this page represents the whole range of emulators that have been containerized and made available to the
EaaSI network by the development team via the OpenSLX team's `GitLab repository <https://gitlab.com/emulation-as-a-service/emulators>`_.

.. warning::
  This does **not** mean that all listed emulators are necessarily available for immediate use in the current node. The presence/availability of the emulator in the node is determined by the "Number of Images".

  For instance, in the above screenshot, the example node contains QEMU (four versions/images), BasiliskII (one version), SheepShaver (one version) and VICE (one version).

Details for each emulator - version number(s), Docker source info, etc. - can be reviewed by clicking the "Details"
button.

.. image:: ../images/emulator_images.png

Again, in the above example, the node installation has four images/versions of QEMU available: 2.5, 2.12, 3.0 and 3.1. This can
be convenient for troubleshooting environments, as certain versions of an emulator may be more compatible with certain
legacy operating systems or hardware than others.

If an emulator has multiple images/versions in a node, the version that EaaSI uses to run an environment can be viewed
and edited on any given environment details page under "Emulator" settings:

.. image:: ../images/emulator_version_settings.png

.. note::
  The "Default" tag will determine which emulator image/version is used when creating new environments with a System
  template that depends on that emulator.

New emulator images/versions can be added to the node in two ways: either through environment replication or manual adding of
Docker images.

Emulator images are automatically imported into a node if a node user attempts to replicate an environment from
a remote node in the network, and the host node does not have the emulator image the remote node used to originally
create and configure that environment.

*For example: a user at Node A sees a remote Mac OS 9.0.4 environment configured using SheepShaver available from Node B.
Node A does not currently have a SheepShaver image installed. But, when the user chooses to replicate, the appropriate
SheepShaver image will be imported as part of the environment replication process, with no extra input needed.
SheepShaver will now be available in Node A for future environment creation and configuration as well.*

The replication method requires little to no direct management from an EaaSI Admin. But Admins can also manually add
emulator images as well by clicking the "Import Emulator" button on the Emulators page:

  .. image:: ../images/import_emulator_button.png

This will bring up the Import Emulator pop-up menu:

  .. image:: ../images/import_emulator_menu.png

Instructions for filling out the Name and Tag fields diverge slightly depending whether the user is attempting to
import a new QEMU image, or any other emulator container:

**QEMU**: EaaS QEMU images are pushed to DockerHub. For the "Name" field, fill in "eaas/qemu-eaas", then use the list
of available Tags on DockerHub to specify the desired QEMU container: `<https://hub.docker.com/r/eaas/qemu-eaas/tags/>`_.

**All others**: All other emulator images are hosted directly on GitLab. To obtain the reference information for a
Docker image, first choose the appropriate emulator from the list of available projects on the EaaS GitLab:
`<https://gitlab.com/emulation-as-a-service/emulators>`_

Once inside the relevant emulator project (e.g. `<https://gitlab.com/emulation-as-a-service/emulators/vice-eaas>`_),
click "Registry" on the left-hand side GitLab menu. From this screen, you can find the link to the containerized
emulator (**use the full registy.gitlab.com address as the "Name" of the emulator in the Import Emulator menu**) and
specific tagged versions, if desired.

.. image:: ../images/container_registry_link.png

.. note::
  If no Tag is specified, the EaaSI import function will default to the "latest" image as specified in each
  emulator's Dockerfile.

The optional "Emulator Alias" field will populate the "User version" information in the emulator's Details page and helps to
differentiate when there are multiple images for any one emulator in a node.

When Name, Tag, and Emulator alias fields have all been filled out as desired, click the "Import Emulator" button.
After a minute or two, the new emulator image should be visible in the Emulators menu and available for creating and
configuring environments.


.. _oai-pmh_management:

Endpoints/Metadata Sync
------------------------

Users can fetch metadata from other nodes on the network (that is, synchronize published environments)
using the "Endpoints/Metadata Sync" settings.

.. image:: ../images/oai-pmh_settings.png

To add endpoints (other nodes) to this page, click the "Add New Endpoint" button.

.. image:: ../images/add_endpoint.png

Details for adding EaaSI Network nodes (i.e. the appropriate Host Location URLs for each node) will be provided to each node's
Admins for configuration. You should use the "Endpoint Name" field to title the endpoint something quick and descriptive to remember the node or organization it represents
(e.g. "yale", "eaasi-open-source-sandbox", etc.)

.. warning::
  The "Endpoint Name" field will automatically validate input to inforce certain naming conventions - particularly whitespaces, certain special characters/symbols (like "*") or capital letters.

  This naming convention is necessary to prevent errors in storage path validation during Environment replication between nodes. If you receive a "Name is invalid" message from the interface, please try removing spaces, capital letters, and/or special symbols before contacting the EaaSI team.

To fetch the available metadata/environments from other nodes, simply click the "Synchronize" button next to the
selected node endpoint. The available "Public" environments on the Explore Resources page should update
accordingly.

Running Tasks
---------------

The Running Tasks tab allows Admins to monitor activity on the node, including any currently running environment sessions,
replication requests, uploads or imports, etc. This high-level information is meant to help Admins
better troubleshoot the node (i.e. confirm that uploads have completed, that environments have been properly shut down,
etc)

.. image:: ../images/running_tasks.png

Install & Updates
-------------------

Using this menu, EaaSI Admins can perform two maintenance tasks that may be necessary following a new update or release of the EaaSI platform.

"Synchronize Backend" will refresh the API connection between EaaSI's front-end and back-end components (recommended after an update to the EaaSI UI)

"Migrate Old DB Data" will convert existing resource metadata following any changes to the node's database.
(recommended to perform only on request by the EaaSI dev team; metadata migrations will only be required after major updates to both front-end and back-end infrastructure)

Troubleshooting
-----------------

In the event that EaaSI users encounter bugs or unexpected behavior, Admin users can use this menu to download server and web API logs. These logs may provide guidance to local IT or to the EaaSI support team. Please attach these logs when :ref:`bugs` if possible.


.. _user_admin:

Node User Administration
=========================

Manage Users
-------------

EaaSI users must for now be added manually by a node :term:`Admin` before they can :ref:`log in <logging_in>` to a node. The Manage Users page lists all individuals that are present on the node, including their given username and their role/permission
level.

.. image:: ../images/node_users.png

Clicking on a user's Details page will allow an Admin the ability to also edit that user's email address (used for authenticating login),
their username and full name information, and their role/permission level:

.. image:: ../images/edit_user.png
  :align: center
  :scale: 75


Create New User
----------------

To add a new user to a node, an Admin can click on the "Create New User" button to set the new user's email,
name, and user role.

**All** fields in the Create New User menu are required.

.. image:: ../images/create_new_user.png
  :align: center
  :scale: 75

Once added, **the user's temporary one-time password will be displayed to the Admin in a notification for approximately 15-20 seconds**. The Admin will need to immediately copy and save the password and provide it to the user, for example:

.. image:: ../images/new_user_temp_password.png
  :align: center


All new users will be prompted the first time they log in to change their password from the provided credentials to a new password of the user's choice. See :ref:`logging_in`.


Resetting User Passwords
--------------------------

If a user (of any level) *forgets* or otherwise loses their password, they will need to ask an Admin-level user in their node to reset their password. Admin users can accomplish this on the Manage Users page of their node settings (Manage Node -> Manage Users -> select Details of user in question):

.. image:: ../images/reset_password.png
  :align: center
  :width: 600

Once the Admin confirms they want to reset the user's password, **the user's new temporary password will be displayed to the Admin in a notification for approximately 15-20 seconds**. The Admin will need to immediately copy and save the password and provide it to the user, for example:

.. image:: ../images/password_notification.png
  :align: center

Once the Admin has provided the user with the temporary password, the user in question will be able to log in again. They will be prompted to immediately change/reset the temporary password back to a password of their choosing.

.. _permissions:

Permission Levels
--------------------

Lower-level ("Configuration") users may be restricted by the EaaSI client from performing certain actions described in this Handbook. To summarize:

**Admin**-level users can:
  - Save public Environments (published from other nodes/sources in the EaaSI Network) to their local node's storage
  - Add new users to the local node and change their permissions level (e.g. promote a Configuration user to an Admin)
  - Publish Environments and Software resources that are local to their node to the wider EaaSI Network
  - Access administrative features, such as OAI-PMH settings, emulator management, etc.
  - All features and actions available to Configuration-level users

**Configuration**-level users can:
  - import new local Software and Content resources to their local node
  - run and create new Environment resources using Environments that have been Saved Locally by an Admin in their local node
  - run and create new Environment resources using public Software resources, or their private Software and Content resources
  - download server logs helpful for troubleshooting and filing bug reports
  - view currently-running tasks in their local node (also potentially useful for filing bug reports)
  - change their basic user account settings (email and password)
