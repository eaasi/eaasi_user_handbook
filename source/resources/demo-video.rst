.. Demo videos

.. include:: ../sunset.rst

Demo Videos
*****************

EaaSI Overview
----------------

  .. raw:: html

    <iframe width="720" height="480" src="https://www.youtube-nocookie.com/embed/IF2Fbovoq-4" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

|
|
|

Running an Environment
-----------------------

  .. raw:: html

    <iframe width="720" height="480" src="https://www.youtube-nocookie.com/embed/1jyJSbQnvY8" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

|
|
|

Importing and Installing Software
-----------------------------------

  .. raw:: html

    <iframe width="720" height="480" src="https://www.youtube-nocookie.com/embed/6fHeXSIT9Fk" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
