.. Links to other docs

.. include:: ../sunset.rst

.. _community:

EaaSI Community
*****************

.. _forum:

EaaSI Community Forum
======================

The `EaaSI Community <https://forum.eaasi.cloud/>`_ Discourse forum is an open space, not just for reporting bugs, but also public discussion regarding emulation, software preservation, configuration workflows, assistance with using legacy software applications, and other topics related to the EaaSI program of work. Please sign up for an account today and join in the conversation!

.. _feedback_form:

Feedback Form 
==============

EaaSI also provides a `Feedback Form <https://forms.office.com/r/gti6Z4W2Za>`_ for providing general comments on the design and reliability of the EaaSI platform. Users are welcome to fill out this form regardless of where and how they have encountered EaaSI; the team welcomes and values all impressions.

(Administrators of EaaSI servers are encouraged to liberally share or link to this Feedback Form as a method of directing their user community's thoughts straight to the EaaSI program team!)

Please note that the EaaSI Feedback Form is collected using Microsoft Forms; Microsoft's `Security and Privacy policies <https://support.microsoft.com/en-us/office/security-and-privacy-in-microsoft-forms-7e57f9ba-4aeb-4b1b-9e21-b75318532cd9>`_ apply. The EaaSI Feedback Form itself collects no personally identifiable information by default (emails, IP addresses, etc.) so the EaaSI team will not see this information unless intentionally provided.


SPN
===

For more information about the EaaSI network and our full program of work, please visit our project site on the
`Software Preservation Network <https://www.softwarepreservationnetwork.org/projects/emulation-as-a-service-infrastructure>`_!
