.. Filing bug reports for EaaSI

.. include:: ../sunset.rst

.. _bugs:

Reporting Bugs
=================

EaaSI users may report bugs and technical issues with the platform directly to the support team in the `Support Center <https://forum.eaasi.cloud/c/support-center/6>`_ on the EaaSI Community Forum. 

(You will need to `register <https://forum.eaasi.cloud/login>`_ for an account on the Community Forum first!) 

New topics in The Support Center are auto-populated with a template to help us gather information on your EaaSI installation to respond as quickly and efficiently as possible to your problem. Please try to fill out as many of the prompts as able or relevant.

Related to bug reports are feature or enhancement requests. We encourage community discussion on suggested features to help us prioritize future development - please post your ideas, and join in the discussion on other user's posts!

Before posting your first question or bug in the EaaSI Community Forum, do please read the `EaaSI Support Guide <https://forum.eaasi.cloud/t/eaasi-support-guide-read-me-first/15>`_ for more details, guidance and expectations. EaaSI provides timely and effective support to the members of the EaaSI Community, but at this time, the EaaSI support team is small in size and capacity. Despite these limitations we strive to ensure users of all types and levels of expertise can put the system to use.
