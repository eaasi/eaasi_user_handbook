.. Development Roadmap

.. include:: ../sunset.rst

.. _roadmap:

EaaSI Product Roadmap
======================

The annual EaaSI Product Roadmap is a high-level summary of both development and maintenance work that has been prioritized for the current calendar year. Work is identified and prioritized for the roadmap according to stakeholder needs. This roadmap is updated quarterly and is intended for a broad audience. To provide feedback for possible future consideration, please reach out to softwarepreservation [at] yale [dot] edu.

The 2024 EAASI Product Roadmap prioritizes features that are widely needed to support a “virtual reading room” use case for archives and special collections. During one-on-one interviews with EAASI users conducted in 2024, access control features emerged as the most important shared priority. Accordingly, the roadmap prioritizes essential work to re-architect EAASI components that enable the access control and permissions workflows needed for the “virtual reading room” use case. This architectural overhaul will bolster EAASI’s stability, durability, and extensibility to support additional use cases.


Prioritized user stories for 2024
----------------------------------

- As an Organization Administrator or Configuration User, I need to make an environment available to all Configuration Users within my Organization (but not beyond my Organization)
- As an Organization Administrator or Configuration User, I need to make a resource available to a specific Access User
- As a Deployment Administrator or Organization Administrator, I need to restrict access to a resource that has been made available to multiple users
- As a System Administrator, I need to designate a Deployment Administrator
- As an Organization Administrator, I need to make a resource available to one or more Organization Administrators who are outside of my Organization but within the same EAASI deployment
- As an Organization Administrator, or Configuration User, I need to make an Environment available to one specific Organization Administrator or Configuration User within my Organization
- As an Organization Administrator, I need to designate Access Users
- As an Organization Administrator, I need to designate Configuration Users
- As a Deployment Administrator, I need to manage Organizations
- As a Deployment Admin, Organization Admin, or Configuration User who is attempting to save edits to a record that another user has concurrently modified, I need the UI to show me an error about the editing conflict and the options I have
- As any user, I need to reset my account password by email
- As a Deployment Administrator, I need to update the branding of the EAASI UI in my deployment
- As an Organization Administrator, I need to know how the EAASI UI does or does not conform to accessibility requirements

The user roles referenced in these stories are defined in the [Definition of EAASI User Types](https://gitlab.com/eaasi/program_docs/rfc/-/blob/main/user-definitions.md). Additional detail about the stories is available on the [2024 EAASI Development GitLab Board](https://gitlab.com/groups/eaasi/-/epic_boards/1052907).

More information about the prioritization of other user stories that are currently planned for after 2024 is available on the [Backlog for Beyond 2024 GitLab Board](https://gitlab.com/groups/eaasi/-/epic_boards/1052910).


How is this roadmap prioritized?
----------------------------------

Yale serves as product management headquarters for the EAASI software and maintains the roadmap. The Yale EAASI product management team is committed to transparently prioritizing the EAASI software roadmap based on shared needs, and Yale encourages EAASI Research Alliance participants to provide input to help prioritize the EAASI software product roadmap.

The product management team is committed to developing and sharing clear mechanisms for routine community voting on the roadmap, code contribution pathways, and documentation to support the continuity of the software should its maintenance or product management evolve over time. The team is also committed to communicating clear expectations about development capacity.

The product management team encourages EAASI Research Alliance participants to provide input on the roadmap starting in 2025 by:
- Reporting potential features via the EAASI User Forum
- Participating in an annual listening session with each institution’s EAASI Alliance Organization Administrator about features that the Alliance community has identified
- Voting in an annual survey about features of interest 

The Alliance Subcommittee will also do an annual “lazy consensus” review of the final prioritized roadmap, in which they can review the roadmap and raise objections if needed.

**The following flowcharts outline the roadmap input pathways in more detail.**

Proposing, reviewing, and gathering feedback on new features that could potentially be added to the EAASI roadmap:

.. image:: ../images/roadmap_new_features.png

Assigning priority to bug fixes, features, and maintenance work, and setting the EAASI roadmap:

.. image:: ../images/roadmap_bug_fixes.png