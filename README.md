# eaasi_user_handbook

## Sunset Notice 01-23-2025

This EaaSI User Handbook respository was in active use during the platform's grant-funded period, 2018-2024. As of 2025, we are revising the Handbook in anticipation of a collaboration with the `Software Preservation Network <https://www.softwarepreservationnetwork.org/>`_ later this year.

As a result, the information in this repository may or may not accurately reflect the expected function and workflows of the EAASI software *moving forward*. This version of the User Handbook will be retired and removed from GitLab Pages in early 2025. Please follow announcements and updates in the `EAASI Forum <https://forum.eaasi.cloud>`_ to learn more about our `product roadmap <https://forum.eaasi.cloud/t/eaasi-product-roadmap/291>`_  or email softwarepreservation@yale.edu for more information about our ongoing development transition!